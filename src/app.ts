import * as express from 'express';
import * as bodyParser from 'body-parser';

import indexRouter from './routes';
import userRouter from './routes/user';

import * as errors from './middlewares/errors';
import requestLogger from './middlewares/requestsLogger';

const app: express.Application = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(requestLogger);

app.use('/api', indexRouter);
app.use('/api/user', userRouter);

app.use(errors.notFound);
app.use(errors.handler);

export default app;
