import { Server, createServer } from 'http';

import app from './app';
import log from './tools/log';

const start = () => {
    const port: string | number = process.env.port || 4000;
    const server: Server = createServer(app);
    server.listen(port, () => {
        log.info(`Server listening on http://localhost:${port}`);
    });
};

export default {
    start
};



