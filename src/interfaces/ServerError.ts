interface ServerError {
    message: string;
    status: number | string;
}

export default ServerError;
