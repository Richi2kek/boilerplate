import { Request, Response, NextFunction } from 'express';
import log from '../tools/log';

const requestLogger = (req: Request, res: Response, next: NextFunction) => {
    switch (req.method) {
        case 'GET':
            log.get(req.url);
            next();
            break;
        case 'POST':
            log.post(req.url);
            next();
            break;
        case 'PUT':
            log.put(req.url);
            next();
            break;
        case 'DELETE':
            log.delete(req.url);
            next();
            break;
        case 'PATCH':
            log.patch(req.url);
            next();
            break;
        case 'HEAD':
            log.head(req.url);
            next();
            break;
        case 'OPTIONS':
            log.options(req.url);
            next();
            break;
    }
};

export default requestLogger;
