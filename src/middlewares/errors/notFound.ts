import { Request, Response, NextFunction } from 'express';

import ServerError from '../../interfaces/ServerError';

export const notFound = (req: Request, res: Response, next: NextFunction) => {
    const err: ServerError = {
        message: `404 NOT FOUND` ,
        status: 404
    };
    next(err);
};

