import { Request, Response, NextFunction } from "express";

import log from '../../tools/log';

export const handler = (err: any, req: Request, res: Response, next: NextFunction) => {
    log.error(err.message);
    res.status(err.status || 500).json(err.message || 'Server Error');
};

