const styles = {
    reset: '\x1b[0m',
    bright: '\x1b[1m',
    dim: '\x1b[2m',
    italic: '\x1b[3m',
    underscore: '\x1b[4m',
    blink: '\x1b[5m',
    reverse: '\x1b[7m',
    hidden: '\x1b[8m',
    strikethrough: '\x1b[9m',

    black: '\x1b[30m',
    blackBright: '\x1b[1;30m',
    blackUnderscored: '\x1b[4;30m',
    blackBrightUnderscored: '\x1b[1;4;30m',

    red: '\x1b[31m',
    redBright: '\x1b[1;31m',
    redUnderscored: '\x1b[4;31m',
    redBrightUnderscored: '\x1b[1;4;31m',

    green: '\x1b[32m',
    greenBright: '\x1b[1;32m',
    greenUnderscored: '\x1b[4;32m',
    greenBrightUnderscored: '\x1b[1;4;32m',

    yellow: '\x1b[33m',
    yellowBright: '\x1b[1;33m',
    yellowUnderscored: '\x1b[4;33m',
    yellowBrightUnderscored: '\x1b[1;4;33m',

    blue: '\x1b[34m',
    blueBright: '\x1b[1;34m',
    blueUnderscored: '\x1b[4;34m',
    blueBrightUnderscored: '\x1b[1;4;34m',

    magenta: '\x1b[35m',
    magentaBright: '\x1b[1;35m',
    magentaUnderscored: '\x1b[4;35m',
    magentaBrightUnderscored: '\x1b[1;4;35m',

    cyan: '\x1b[36m',
    cyanBright: '\x1b[1;36m',
    cyanUnderscored: '\x1b[4;36m',
    cyanBrightUnderscored: '\x1b[1;4;36m',

    grey: '\x1b[90m',
    greyBright: '\x1b[1;90m',
    greyUnderscored: '\x1b[4;90m',
    greyBrightUnderscored: '\x1b[1;4;90m',

    white: '\x1b[37m',

    bgBlack: '\x1b[40m',
    bgRed: '\x1b[41m',
    bgGreen: '\x1b[42m',
    bgYellow: '\x1b[43m',
    bgBlue: '\x1b[44m',
    bgMagenta: '\x1b[45m',
    bgCyan: '\x1b[46m',
    bgWhite: '\x1b[47m',

    error: '\x1b[1;7;31;40m',
    info: '\x1b[1;7;32;40m',
    warning: '\x1b[1;7;33;40m',
};

const log = {
    info: (msg: any) => {
        msg = msg || '';
        console.log(`${styles.info}${styles.underscore}[INFO]${styles.reset}${styles.info}:${styles.reset}${styles.greenBright} ${msg}${styles.reset}`);
    },
    error: (err: any) => {
        err = err || '';
        console.log(`${styles.error}${styles.underscore}[ERROR]${styles.reset}${styles.error}:${styles.reset}${styles.redBright} ${err}${styles.reset}`);
    },
    warning: (warning: any) => {
        warning = warning || '';
        console.log(`${styles.warning}${styles.underscore}[WARNING]${styles.reset}${styles.warning}:${styles.reset}${styles.yellowBright} ${warning}${styles.reset}`);
    },
    get: (get: any) => {
        get = get || '';
        console.log(`${styles.blue}${styles.underscore}[GET]${styles.reset}${styles.blueBright}: ${get}${styles.reset}`);
    },
    post: (post: any) => {
        post = post || '';
        console.log(`${styles.green}${styles.underscore}[POST]${styles.reset}${styles.greenBright}: ${post}${styles.reset}`);
    },
    put: (put: any) => {
        put = put || '';
        console.log(`${styles.magenta}${styles.underscore}[PUT]${styles.reset}${styles.magentaBright}: ${put}${styles.reset}`);
    },
    patch: (patch: any) => {
        patch = patch || '';
        console.log(`${styles.yellow}${styles.underscore}[PATCH]${styles.reset}${styles.yellowBright}: ${patch}${styles.reset}`);
    },
    delete: (msg: any) => {
        msg = msg || '';
        console.log(`${styles.red}${styles.underscore}[DELETE]${styles.reset}${styles.redBright}: ${msg}${styles.reset}`);
    },
    options: (options: any) => {
        options = options || '';
        console.log(`${styles.cyan}${styles.underscore}[OPTIONS]${styles.reset}${styles.cyanBright}: ${options}${styles.reset}`);
    },
    head: (head: any) => {
        head = head || '';
        console.log(`${styles.grey}${styles.underscore}[HEAD]${styles.reset}${styles.greyBright}: ${head}${styles.reset}`);
    },
};

export default log;
