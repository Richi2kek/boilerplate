import { Router, Request, Response } from 'express';

const userRouter: Router = Router();

userRouter.get('/', (req: Request, res: Response) => {
    res.json('ok user');
});

export default userRouter;
