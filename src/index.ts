import server from './server';
import log from './tools/log';
try {
    log.info('Application starting...');
    server.start();
} catch (err) {
    log.error(err);
}


